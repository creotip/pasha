import React from 'react'
import PropTypes from 'prop-types'
import Link from 'gatsby-link'
import Helmet from 'react-helmet'
import Header from '../components/Header'
import Footer from '../components/Footer'
import './index.scss'
import '../styles/myant.less'

class TemplateWrapper extends React.Component {
    render() {
        const { location, children } = this.props
        return (
            <div dir={'rtl'}>
                <Helmet
                    title="Pavel Isakov Website"
                    meta={[
                        {name: 'description', content: 'Sample'},
                        {name: 'keywords', content: 'sample, something'},
                    ]}
                />
                <Header {...this.props} />
                <div
                    className={'wrapper'}
                >
                    {children()}
                </div>
                <Footer />
            </div>
        )
    }
}

export default TemplateWrapper
