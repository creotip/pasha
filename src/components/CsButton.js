import React from 'react'
import Button from 'antd/lib/button';

export const RedButton = ({size}) => (
    <Button size={size} type="primary">Button</Button>
)