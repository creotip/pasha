import React from 'react'
import PropTypes from 'prop-types'
import Link from 'gatsby-link'
import Logo from '../assets/ecp-logo.svg'

class Header extends React.Component {
    render() {
        const { location, title } = this.props
        return (
            <header className={`header ${ location.pathname === '/' ? 'home' : 'inner-pages'}`}>
                <div className="container">
                    <nav className="navbar navbar-expand-lg navbar-light bg-light">
                        <a className="navbar-brand" href="#">
                            <img src={Logo} alt="Ecp logo"/>
                        </a>
                        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                            <span className="navbar-toggler-icon"></span>
                        </button>
                        <div className="collapse navbar-collapse" id="navbarText">
                            <ul className="navbar-nav mr-auto">
                                <li
                                    className={ location.pathname === '/' ? 'nav-item active' : 'nav-item'}
                                >
                                    <Link to="/" className="nav-link">
                                        ראשי
                                    </Link>
                                </li>
                                <li className={ location.pathname === '/about/' ? 'nav-item active' : 'nav-item'}>
                                    <Link to="/about/" className="nav-link">
                                        אודות
                                    </Link>
                                </li>
                                <li className={ location.pathname === '/contact/' ? 'nav-item active' : 'nav-item'}>
                                    <Link to="/contact/" className="nav-link">
                                        צור קשר
                                    </Link>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>

            </header>
        )
    }
}

export default Header