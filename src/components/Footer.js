import React from 'react'
import PropTypes from 'prop-types'
import Link from 'gatsby-link'

class Footer extends React.Component {
    render() {
        const { location, title } = this.props
        return (
            <footer className="footer text-center">
                <div className="container">
                    <span className="text-muted">
                        כל הזכויות שמורות לפאשה
                    </span>
                </div>
            </footer>
        )
    }
}

export default Footer