import React from 'react'
import Link from 'gatsby-link'
import Banner from '../assets/2.jpg'

const IndexPage = () => (
    <div className={'content'}>
        <div className={'banner'}>
            <img src={Banner} alt=""/>
        </div>
        <div className="container">
            <h1 className='text-center'>חברת הבנייה פאשה</h1>

                <div className="row">
                    <div className="col-sm">
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad amet assumenda beatae commodi
                            dolor, earum hic ipsum minima molestias nemo quam saepe sed tempora temporibus veniam.
                            Consequuntur officia sequi voluptatum.

                        </p>
                    </div>
                </div>

                <div className="row">
                    <div className="col-sm">
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad autem consequuntur in
                            laboriosam magni necessitatibus nemo nisi numquam placeat porro praesentium quae quam,
                            quasi, quia sunt unde voluptas! Impedit, quibusdam.
                        </p>
                    </div>
                    <div className="col-sm">
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad autem consequuntur in
                            laboriosam magni necessitatibus nemo nisi numquam placeat porro praesentium quae quam,
                            quasi, quia sunt unde voluptas! Impedit, quibusdam.
                        </p>
                    </div>
                </div>

        </div>
    </div>
)

export default IndexPage
