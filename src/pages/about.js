import React from 'react'
import Link from 'gatsby-link'
// import {Button} from 'semantic-ui-react'
import {RedButton} from '../components/CsButton'

const About = () => (
    <div className='container'>
        <RedButton size={'large'} />
        <br/>
        <br/>
        <p>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam aspernatur assumenda deserunt error
            exercitationem fuga fugit ipsum molestias, mollitia, necessitatibus neque, nihil pariatur quam sit sunt
            temporibus veniam vitae voluptatibus!
        </p>
    </div>
)

export default About
