module.exports = {
  siteMetadata: {
    title: `Pavel Isakov Website`,
  },
  plugins: [
      `gatsby-plugin-react-helmet`,
      `gatsby-plugin-sass`,
      `gatsby-plugin-less`
  ],
}
